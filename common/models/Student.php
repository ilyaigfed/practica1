<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 06.06.2018
 * Time: 16:35
 */

namespace common\models;


use yii\db\ActiveRecord;

class Student extends ActiveRecord
{
    public function rules()
    {
        return [
            [['name', 'study_date', 'group_id', 'university_id'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'study_date' => 'Дата поступления',
            'group_id' => 'Группа',
            'university_id' => 'Университет',
        ];
    }
}