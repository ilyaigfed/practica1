<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 06.06.2018
 * Time: 15:10
 */

namespace common\models;


use yii\db\ActiveRecord;

class Group extends ActiveRecord
{
    public function rules()
    {
        return [
            [['title', 'profile', 'year', 'university_id'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'profile' => 'Специальность',
            'year' => 'Курс',
            'university_id' => 'Университет',
        ];
    }

    static public function generateTitleList()
    {
        $groups = self::find()->select(['id', 'title'])->all();
        $array = [];
        foreach($groups as $g) {
            $array[$g->id] = $g->title;
        }
        return $array;
    }
}