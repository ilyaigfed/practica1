<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 06.06.2018
 * Time: 14:12
 */

namespace common\models;


use yii\db\ActiveRecord;

class University extends ActiveRecord
{
    public function rules()
    {
        return [
            [["title", "adress", "rector", "site", "ur_adress"], "required"],
        ];
    }

    public function attributeLabels()
    {
        return [
            "title" => 'Название',
            "adress" => 'Адрес',
            "rector" => 'Ректор',
            "site" => 'Веб-сайт',
            "ur_adress" => 'Юр. адрес',
        ];
    }

    static public function generateTitleList()
    {
        $univers = self::find()->select(['id', 'title'])->all();
        $array = [];
        foreach($univers as $u) {
            $array[$u->id] = $u->title;
        }
        return $array;
    }
}