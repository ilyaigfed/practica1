<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m180612_103637_create_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'profile' => $this->string(),
            'year' => $this->integer(),
            'university_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('group');
    }
}
