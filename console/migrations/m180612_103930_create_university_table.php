<?php

use yii\db\Migration;

/**
 * Handles the creation of table `university`.
 */
class m180612_103930_create_university_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('university', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'adress' => $this->string(),
            'rector' => $this->string(),
            'site' => $this->string(),
            'ur_adress' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('university');
    }
}
