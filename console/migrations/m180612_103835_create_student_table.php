<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m180612_103835_create_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('student', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'study_date' => $this->date(),
            'group_id' => $this->integer(),
            'university_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('student');
    }
}
