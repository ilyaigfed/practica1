<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

$this->title = 'Студенты';

?>

<?= Html::a('Сбросить фильтры','/', ['class' => 'btn btn-primary']) ?>
<?php Pjax::begin() ?>
<table class="table">
    <tr>
        <th>ФИО</th>
        <th>Дата поступления</th>
        <th>Группа</th>
        <th>Университет</th>
    </tr>
    <?php foreach($students as $s): ?>
        <tr>
            <td><?= $s->name ?></td>
            <td><?= $s->study_date ?></td>
            <td><?= Html::a($s->group_id, Url::to(['site/index', 'sort_gro' => $s->group_id])) ?></td>
            <td><?= Html::a($s->university_id, Url::to(['site/index', 'sort_uni' => $s->university_id])) ?></td>
        </tr>
    <?php endforeach ?>
</table>
<?= LinkPager::widget(['pagination' => $pages]) ?>
<?php Pjax::end() ?>
