<?php
namespace backend\controllers;

use common\models\Group;
use common\models\Student;
use common\models\University;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'newuniversity',
                            'university',
                            'deleteuniversity',
                            'newgroup',
                            'group',
                            'deletegroup',
                            'newstudent',
                            'student',
                            'deletestudent',
                        ],
                        'allow' => true,
                        'matchCallback' => function() {
                            if(isset(Yii::$app->user->identity->role)) {
                                if(Yii::$app->user->identity->role !== "admin") {
                                    return false;
                                } else return true;
                            } else return false;
                        }
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->accessControl()) {
            $model->login();
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    //универы
    public function actionUniversity()
    {
        $query = University::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3]);
        $uni = $query->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('university', compact(['pages', 'uni']));
    }

    public function actionNewuniversity()
    {
        $model = new University();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['site/university']);
        }

        return $this->render('newuniversity', compact(['model']));
    }

    public function actionDeleteuniversity($id = null)
    {
        University::deleteAll(['id' => $id]);
        return $this->redirect(['site/university']);
    }

    //группы
    public function actionNewgroup()
    {
        $model = new Group();
        $uni = University::generateTitleList();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['site/group']);
        }

        return $this->render('newgroup', compact(['model', 'uni']));
    }

    public function actionGroup()
    {
        $query = Group::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3]);
        $groups = $query->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();
        foreach($groups as $g) {
            $u = University::findOne(['id' => $g->university_id]);
            $g->university_id = $u->title;
        }
        return $this->render('group', compact(['pages', 'groups']));
    }

    public function actionDeletegroup($id = null)
    {
        Group::deleteAll(['id' => $id]);
        return $this->redirect(['site/group']);
    }

    //студенты
    public function actionNewstudent()
    {
        $model = new Student();
        $uni = University::generateTitleList();
        $gro = Group::generateTitleList();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['site/student']);
        }

        return $this->render('newstudent', compact(['model', 'uni', 'gro']));
    }

    public function actionStudent()
    {
        $query = Student::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3]);
        $students = $query->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();
        foreach($students as $s) {
            $u = University::findOne(['id' => $s->university_id]);
            $g = Group::findOne(['id' => $s->group_id]);
            $s->university_id = $u->title;
            $s->group_id = $g->title;
        }
        return $this->render('student', compact(['pages', 'students']));
    }

    public function actionDeletestudent($id = null)
    {
        Student::deleteAll(['id' => $id]);
        return $this->redirect(['site/student']);
    }
}