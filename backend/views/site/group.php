<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

$this->title = 'Группы';

?>

<?= Html::a('Добавить', ['site/newgroup'], ['class' => 'btn btn-primary']) ?>
<?php Pjax::begin() ?>
    <table class="table">
        <tr>
            <th>Название</th>
            <th>Специальность</th>
            <th>Курс</th>
            <th>Университет</th>
            <th>Опции</th>
        </tr>
        <?php foreach($groups as $g): ?>
            <tr>
                <td><?= $g->title ?></td>
                <td><?= $g->profile ?></td>
                <td><?= $g->year ?></td>
                <td><?= $g->university_id ?></a></td>
                <td><?= Html::a('', Url::to(['site/deletegroup', 'id' => $g->id]), ['class' => 'glyphicon glyphicon-remove']) ?></td>
            </tr>
        <?php endforeach ?>
    </table>
<?= LinkPager::widget(['pagination' => $pages]) ?>
<?php Pjax::end() ?>