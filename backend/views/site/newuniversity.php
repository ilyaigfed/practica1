<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Университеты - Добавить';

?>

<div class="col-md-4">
    <?php $form =  ActiveForm::begin() ?>
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'adress')->textInput() ?>
    <?= $form->field($model, 'rector')->textInput() ?>
    <?= $form->field($model, 'site')->textInput() ?>
    <?= $form->field($model, 'ur_adress')->textarea() ?>
    <?= Html::submitButton("Добавить", ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
</div>