<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

$this->title = 'Студенты';

?>

<?= Html::a('Добавить', ['site/newstudent'], ['class' => 'btn btn-primary']) ?>
<?php Pjax::begin() ?>
    <table class="table">
        <tr>
            <th>ФИО</th>
            <th>Дата поступления</th>
            <th>Группа</th>
            <th>Университет</th>
            <th>Опции</th>
        </tr>
        <?php foreach($students as $s): ?>
            <tr>
                <td><?= $s->name ?></td>
                <td><?= $s->study_date ?></td>
                <td><?= $s->group_id ?></td>
                <td><?= $s->university_id ?></a></td>
                <td><?= Html::a('', Url::to(['site/deletestudent', 'id' => $s->id]), ['class' => 'glyphicon glyphicon-remove']) ?></td>
            </tr>
        <?php endforeach ?>
    </table>
<?= LinkPager::widget(['pagination' => $pages]) ?>
<?php Pjax::end() ?>