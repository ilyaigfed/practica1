<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

$this->title = 'Университеты';

?>

<?= Html::a('Добавить', ['site/newuniversity'], ['class' => 'btn btn-primary']) ?>
<?php Pjax::begin() ?>
<table class="table">
    <tr>
        <th>Название</th>
        <th>Адрес</th>
        <th>Ректор</th>
        <th>Веб-сайт</th>
        <th>Юр. адрес</th>
        <th>Опции</th>
    </tr>
    <?php foreach($uni as $u): ?>
        <tr>
            <td><?= $u->title ?></td>
            <td><?= $u->adress ?></td>
            <td><?= $u->rector ?></td>
            <td><a href="<?= $u->site ?>"><?= $u->site ?></a></td>
            <td><?= $u->ur_adress ?></td>
            <td><?= Html::a('', Url::to(['site/deleteuniversity', 'id' => $u->id]), ['class' => 'glyphicon glyphicon-remove']) ?></td>
        </tr>
    <?php endforeach ?>
</table>
<?= LinkPager::widget(['pagination' => $pages]) ?>
<?php Pjax::end() ?>

