<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Студенты - Добавить';

?>

<div class="col-md-4">
    <?php $form =  ActiveForm::begin() ?>
    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'study_date')->input('date') ?>
    <?= $form->field($model, 'group_id')->dropDownList($gro) ?>
    <?= $form->field($model, 'university_id')->dropDownList($uni) ?>
    <?= Html::submitButton("Добавить", ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
</div>