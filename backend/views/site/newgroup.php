<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Группы - Добавить';

?>

<div class="col-md-4">
    <?php $form =  ActiveForm::begin() ?>
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'profile')->textInput() ?>
    <?= $form->field($model, 'year')->input('number') ?>
    <?= $form->field($model, 'university_id')->dropDownList($uni) ?>
    <?= Html::submitButton("Добавить", ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
</div>